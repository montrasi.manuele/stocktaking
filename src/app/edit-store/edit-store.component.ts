import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { OrderProductModel } from '../service/OrderProduct.model';
import { OrderService } from '../service/OrderService.model';
import { Product } from '../../assets/ObjectDb/Product';
import { ProductService } from '../service/ProductService.model';
import { OrderProduct } from '../../assets/ObjectDb/OrderProduct';
import { Order } from '../../assets/ObjectDb/Order';
import { ShoplistProduct } from '../../assets/ObjectDb/ShoplistProduct';


@Component({
  selector: 'app-edit-store',
  templateUrl: './edit-store.component.html',
  styleUrls: ['./edit-store.component.css']
})
export class EditStoreComponent {

  myControl = new FormControl();
  filteredOptionsProduct: Observable<Product[]>;
  isReadOnly: boolean;
  dataProduct: Product[];
  idProduct: number;
  objData: OrderProduct;
  result: String;

  infoData = this.formBuilder.group({
    listname: ['', Validators.required],
    totprice: [''],
    name: ['', Validators.required],
    quantity: ['', Validators.required],
    expiration_order: [''],
    price: [''],
    completed: [false]
  });

  constructor(private formBuilder: FormBuilder, private orderProductService: OrderProductModel, private productService: ProductService, private orderService: OrderService) {
    this.isReadOnly = true;

    this.productService.getAllProduct().subscribe((dataProduct) => {
      this.dataProduct = dataProduct;
      this.filteredOptionsProduct = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(valueProduct => this._filterProduct(valueProduct))
        );
    });
  }

  private _filterProduct(valueProduct: string): Product[] {
    const filterValueProduct = valueProduct.toLowerCase();
    return this.dataProduct.filter(optionProduct => optionProduct.name.toLowerCase().includes(filterValueProduct));
  }

  setValueProduct(product: Product) {
    this.idProduct = product.id;
    this.infoData.controls['name'].setValue(product.name);
    this.infoData.controls['price'].setValue(product.price);
    this.isReadOnly = false;
  }

  setValueOrder(order: Order) {
    this.infoData.controls['totprice'].setValue(order.price_store);
  }

  setQuantity(quantity: number): void {
    this.infoData.controls['totprice'].setValue(quantity * this.infoData.value.price);
  }

  sendData() {
    this.objData = this.infoData.value;
    this.objData.id_product = this.idProduct;
    this.orderProductService.postOrderProduct(this.objData).subscribe((data) => {
      console.log(data);
      if (data != null) {
        this.result = "Product added in list";
      } else {
        this.result = "ERROR!";
      }
    });
  }

  clearForm() {
    this.infoData.reset();
    this.isReadOnly = true;
    this.infoData.controls['totprice'].setValue('');
  }

}