import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { OrderService } from '../service/OrderService.model';
import { ProductService } from '../service/ProductService.model';
import { FormBuilder, FormControl } from '@angular/forms';
import { Order } from 'src/assets/ObjectDb/Order';
import { ProductQuantity } from 'src/assets/ObjectDb/ProductQuantity';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-order-store',
  templateUrl: './order-store.component.html',
  styleUrls: ['./order-store.component.css']
})
export class OrderStoreComponent implements OnInit {
  displayedColumns = ['name', 'description', 'price', 'quantity'];
  dataSource = new MatTableDataSource<ProductQuantity>();
  listInfoDataOrder: Order[];
  infoDataOrder: Order;
  myControl = new FormControl();
  filteredOptions: Observable<Order[]>;
  isShowed: boolean;

  myForm = this.formBuilder.group({
    listname: ['']
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private orderService: OrderService, private productService: ProductService, private formBuilder: FormBuilder) {
    this.isShowed = false;
    this.orderService.getAllOrder().subscribe((data) => {
      this.listInfoDataOrder = data;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
    });
  }

  ngOnInit(): void {
  }

  private _filter(value: string): Order[] {
    const filterValue = value.toLowerCase();
    return this.listInfoDataOrder.filter(option => option.listname.toLowerCase().includes(filterValue));
  }

  setValue(order: Order) {
    this.infoDataOrder = order;
    this.isShowed = true;
    this.myForm.controls['listname'].disable();
    this.productService.getAllProductById(order.id).subscribe((data) => {
      console.log('data: ' + data);
      this.dataSource.data = data;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  clearForm() {
    this.infoDataOrder = null;
    this.isShowed = false;
    this.myForm.controls['listname'].enable();
    this.myForm.controls['listname'].setValue("");
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }
}