import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderStoreComponent } from './order-store/order-store.component';
import { ProductStoreComponent } from './product-store/product-store.component';
import { EditStoreComponent } from './edit-store/edit-store.component';
import { EditProductComponent } from './edit-product/edit-product.component';


const routes: Routes = [
  { path: 'orderStore', component: OrderStoreComponent },
  { path: 'productStore', component: ProductStoreComponent },
  { path: 'editStore', component: EditStoreComponent },
  { path: 'editProduct', component: EditProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
