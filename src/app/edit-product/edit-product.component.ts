import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../service/ProductService.model';
import { Product } from '../../assets/ObjectDb/Product';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  result: string;

  constructor(private formBuilder: FormBuilder, private productSerice: ProductService) { }

  infoData = this.formBuilder.group({
    name: ['', Validators.required],
    description: [''],
    price: ['', Validators.required]
  });

  sendData() {
    this.productSerice.postProduct(this.infoData.value).subscribe((data) => {
      if (data != null) {
        this.result = "Product inserted";
      } else {
        this.result = "Product already existing in the product list";
      }
    });
  }

  clearForm() {
    this.infoData.reset();
  }

  ngOnInit(): void {
  }

}
