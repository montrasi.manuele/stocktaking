import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductStoreComponent } from './product-store/product-store.component';
import { OrderStoreComponent } from './order-store/order-store.component';
import { EditStoreComponent } from './edit-store/edit-store.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { MaterialWrapper } from './material-wrapper';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";

@NgModule({ 
  declarations: [
    AppComponent,
    ProductStoreComponent,
    OrderStoreComponent,
    EditStoreComponent,
    EditProductComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialWrapper,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
