import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Product } from '../../assets/ObjectDb/Product';
import { ProductService } from '../service/ProductService.model';

@Component({
  selector: 'app-product-store',
  templateUrl: './product-store.component.html',
  styleUrls: ['./product-store.component.css']
})
export class ProductStoreComponent implements OnInit {
  displayedColumns = ['name', 'description', 'price', 'edit'];
  dataSource = new MatTableDataSource<Product>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private productService: ProductService) {
    this.loadDataTable();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  onClickDelete(element) {
    this.productService.deleteProduct(element.id).subscribe((data) => {
      this.loadDataTable();
    });
  }

  private loadDataTable() {
    this.productService.getAllProduct().subscribe((data) => {
      this.dataSource.data = data;
    });
  }

}
