import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../../assets/ObjectDb/Product';
import { ProductQuantity } from 'src/assets/ObjectDb/ProductQuantity';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  // GET
  getAllProduct(): Observable<Array<Product>> {
    return this.http.get<Array<Product>>('http://localhost:8087/api/getAllproducts');
  }

  getProduct(): Observable<Product> {
    return this.http.get<Product>('http://localhost:8080/getProduct');
  }

  getProductByFilter(filter: object): Observable<Product> {
    return this.http.get<Product>('http://localhost:8080/getProductByFilter', filter);
  }

  getAllProductById(idOrder: number): Observable<Array<ProductQuantity>> {
    return this.http.get<Array<ProductQuantity>>('http://localhost:8087/api/getAllProductById/' + idOrder);
  }



  // POST
  postProduct(product: Product): Observable<Product> {
    return this.http.post<Product>('http://localhost:8087/api/postProduct', product);
  }

  // DELETE
  deleteProduct(id: number): any {
    return this.http.delete<any>('http://localhost:8087/api/deleteProduct/' + id);
  }

}