import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from '../../assets/ObjectDb/Order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {
  }

  // GET
  getAllOrder(): Observable<Array<Order>> {
    return this.http.get<Array<Order>>('http://localhost:8087/api/getAllOrder');
  }

  getOrderByListname(listname: string): Observable<Order> {
    return this.http.get<Order>('http://localhost:8087/api/getOrderByListname/' + listname);
  }

  getOrderByFilter(filter: object): Observable<Order> {
    return this.http.get<Order>('http://localhost:8080/api/getOrderByFilter', filter);
  }



  // POST
  postOrder(product: Order): Observable<Order> {
    return this.http.post<Order>('http://localhost:8080/api/postOrder', product);
  }



  // DELETE
  deleteOrder(idOrder: number): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8080/api/deleteOrder', idOrder);
  }

}