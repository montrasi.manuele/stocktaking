import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderProduct } from '../../assets/ObjectDb/OrderProduct';
import { ShoplistProduct } from '../../assets/ObjectDb/ShoplistProduct';

@Injectable({
  providedIn: 'root'
})
export class OrderProductModel {

  constructor(private http: HttpClient) {
  }

  // GET
  getAllOrderProduct(): Observable<Array<OrderProduct>> {
    return this.http.get<Array<OrderProduct>>('http://localhost:8080/getAllOrderProduct');
  }

  getOrderProduct(): Observable<OrderProduct> {
    return this.http.get<OrderProduct>('http://localhost:8080/getOrderProduct');
  }

  getOrderProductByFilter(filter: object): Observable<OrderProduct> {
    return this.http.get<OrderProduct>('http://localhost:8080/getOrderProductByFilter', filter);
  }



  // POST
  postOrderProduct(orderProduct: OrderProduct): Observable<ShoplistProduct> {
    return this.http.put<ShoplistProduct>('http://localhost:8087/api/postOrderProduct', orderProduct);
  }



  // DELETE
  deleteOrderProduct(idOrderProduct: number): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8080/deleteOrderProduct', idOrderProduct);
  }

}