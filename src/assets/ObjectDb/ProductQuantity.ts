export interface ProductQuantity {
    id: number;
    name: string;
    description: string;
    price: number;
    quantity: number;
}