export interface ShoplistProduct {
    id: number;
    id_shoplist: number;
    id_product: number;
    quantity: number;
}