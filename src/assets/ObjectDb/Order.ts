export interface Order {
  id: number;
  listname: string;
  date_order: Date;
  expiration_date: Date;
  completed: boolean;
  price_store: number;
}