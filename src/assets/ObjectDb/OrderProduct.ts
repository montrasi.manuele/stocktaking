export interface OrderProduct {
    id_order: number;
    listname: string;
    date_order: Date;
    expiration_order: Date;
    completed: boolean;
    id_product: number;
    name: string;
    description: string;
    price: number;
    quantity: number;
}